import json
import string
import gzip
import requests
from shutil import copyfile
from time import time
from os import makedirs
from pathlib import Path
from tqdm import tqdm
from logging import debug
from glob import glob
from datetime import datetime
from tempfile import NamedTemporaryFile
from typing import List

MAX_ITEMS_FOR_FRAGMENT = 20
ALLOWED_CHARACTERS = string.ascii_lowercase + string.digits + '!' + '$' + '#' + '?' + '.'
DATA_DIR = './data/search'
MAPPINGS_DIR = './mappings/search'
SEARCH_KG_DIR = './kg/search'
DAILY_EXPORT_URL = f'http://files.tmdb.org/p/exports/tv_series_ids_{datetime.now().strftime("%m_%d_%Y")}.json.gz'
EXPORT_ARCHIVE = 'tv_series_ids.json.gz'

def create_fragment(key: str, data: dict):
    p: Path = Path(f'{DATA_DIR}/{key}.json')
    debug(f'Creating fragment: {p}')

    if p.exists():
        add_to_fragment(key, data)
    else:
        with open(str(p), 'w') as f:
            json.dump(data, f, indent=4, sort_keys=True)

def add_to_fragment(key: str, data: dict):
    p: Path = Path(f'{DATA_DIR}/{key}.json')
    debug(f'Adding to fragment: {p}')

    if not p.exists():
        raise Exception(f'File does not exist: {p}')

    with open(str(p), 'r') as f1:
        content = json.load(f1)
        content['data'] += data['data']
        content['data'] = sorted(content['data'], reverse=True,
                                 key=lambda d: d['popularity'])
        top20 = content['top20'] + data['top20']
        top20 = [dict(t) for t in {tuple(d.items()) for d in top20}]
        top20 = sorted(top20, reverse=True, key=lambda d: d['popularity'])
        content['top20'] = top20[:MAX_ITEMS_FOR_FRAGMENT]
        content['relations'] += data['relations']
        with open(str(p), 'w') as f2:
            json.dump(content, f2, indent=4, sort_keys=True)

def generate_fragmentation():
    old_show_ids = []
    show_ids = []
    shows = []
    fragmentation_keys = []
    frag = {}

    # Load show IDs of processed shows
    # Remove this archive if you want to build the dataset from scratch
    if Path(EXPORT_ARCHIVE).exists():
        print('Found processed show IDs, reading...')
        with gzip.open(EXPORT_ARCHIVE, 'r') as f:
            for line in f:
                data = json.loads(line)
                if not data['original_name'] or not data['id']:
                    continue
                old_show_ids.append(data['id'])

    # Load shows
    print(f'Retrieve latest dump: {DAILY_EXPORT_URL}')
    response: Response = requests.get(DAILY_EXPORT_URL, stream=True)
    archive: NamedTemporaryFile = NamedTemporaryFile()
    with open(archive.name, 'wb') as f:
        for chunk in response:
            f.write(chunk)

    print('Latest dump fetched, reading...')
    with gzip.open(archive.name, 'r') as f:
        for line in f:
            data = json.loads(line)
            if not data['original_name'] or not data['id']:
                continue
            shows.append(data)

    # Remove processed shows from generation list
    old_show_ids = set(old_show_ids)
    shows = [x for x in shows if x['id'] not in old_show_ids]

    # No shows are updated, skipping
    if not shows:
        print('No shows to process, aborting...')
        return []
    print(f'Found {len(shows)} new shows')

    # Create data dir if not exist
    makedirs(DATA_DIR, exist_ok=True)

    # Create fragmentation
    for s in tqdm(shows):
        for char in ALLOWED_CHARACTERS:
            if char in s['original_name'].lower():
                if char not in fragmentation_keys:
                    fragmentation_keys.append(char)
                if char not in frag:
                    frag[char] = {'relations': [], 'data': [], 'top20': []}
                frag[char]['data'].append(s)

    # Refragment
    level = 1
    while(True):
        t = time()
        performed_fragmentation = False

        for key in tqdm(list(frag.keys())):
            content = frag[key]
            data = content['data']
            performed_fragmentation = True
            relations = []

            # Only modify current level
            if len(key) != level:
                continue

            # Fragment all keys with content > MAX_ITEMS_FOR_FRAGMENT
            if len(data) <= MAX_ITEMS_FOR_FRAGMENT:
                continue

            # Put each show in all possible fragments
            # Shows here already match with the value of key since this is
            # enforced in the previous iteration
            for show in data:
                name = show['original_name']
                for char in filter(lambda x: (key + x) in name.lower(),
                                   ALLOWED_CHARACTERS):
                    char1 = key + char # contains('ab')

                    if char1 in name.lower():
                        if char1 not in frag:
                            frag[char1] = { 'relations': [], 'data': [], 'top20': [] }
                            relations.append({ 'character': char1 })
                        frag[char1]['data'].append(show)

            # Keep shows which exactly match with the key such as NCIS
            exact_matches = filter(lambda x: x['original_name'].lower() == key,
                                   data)
            exact_matches = list(exact_matches)

            # Sort by popularity
            most_popular = sorted(data, reverse=True,
                                  key=lambda d: d['popularity'])

            # Take the exact matches appended with the most popular shows
            top20 = exact_matches + most_popular[:MAX_ITEMS_FOR_FRAGMENT]
            top20 = [dict(t) for t in {tuple(d.items()) for d in top20}]
            top20 = sorted(top20, reverse=True, key=lambda d: d['popularity'])
            content['top20'] = top20[:MAX_ITEMS_FOR_FRAGMENT]

            if key == 'nt.':
                print(f'DATA={data}')
                print(f'EXACT={exact_matches}')
                print(f'EXACT={most_popular}')
                print(f'TOP20={top20}')

            # Store relations
            for r in relations:
                content['relations'].append(r)

        # If we haven't made any relations anymore for this level, stop
        if not list(filter(lambda x: len(x) == level and frag[x]['relations'],
                           frag.keys())):
            print('All nodes in level are leafs, stop fragmentation')
            break

        print(f'Completed fragmentation round {level} in {(time() - t):.2f}s')

        # Safety measure
        if not performed_fragmentation:
            raise Exception('No fragmentation is performed in this round, '
                            'this should not happen!')
        level += 1

    print('Fragmentation finished! Exporting fragments to files...')
    for key in tqdm(frag.keys()):
        # Copy leaf's data into top20 as that happens when the node is
        # fragmented, but leafs are not fragmented anymore because they are
        # leafs.
        if not frag[key]['top20']:
            if len(frag[key]['data']) <= MAX_ITEMS_FOR_FRAGMENT:
                frag[key]['top20'] = frag[key]['data']
            else:
                raise Exception(f'Leaf {key} has more than'
                                 'MAX_ITEMS_FOR_FRAGMENT, this should not '
                                 'happen!')

        # Write to disk
        Path(f'{SEARCH_KG_DIR}/{key}.gz').unlink(missing_ok=True)
        create_fragment(key, frag[key])

    copyfile(archive.name, EXPORT_ARCHIVE)
    return frag.keys()

# Generate mappings
def generate_mappings(keys: List[str]):
    # No fragments are changed, skipping generation
    if not keys:
        print('No mappings to generate, aborting...')
        return
    print(f'Generating mappings for {len(keys)} fragments...')

    # Create data dir if not exist
    makedirs(f'{MAPPINGS_DIR}/generated', exist_ok=True)

    # Copy search index
    copyfile('INDEX.json', f'{DATA_DIR}/INDEX.json')
    keys = list(keys)
    keys.append('INDEX')

    # Generate mappings for each JSON file
    for character in tqdm(keys):
        path: str = f'{MAPPINGS_DIR}/char_template.rml.ttl'

        # Handle search index edge case
        if character == 'INDEX':
            path = f'{MAPPINGS_DIR}/index_template.rml.ttl'

        with open(path, 'r') as f1:
            lines = f1.readlines()
            path2: str = f'{MAPPINGS_DIR}/generated/{character}.rml.ttl'
            with open(path2, 'w') as f2:
                for l in lines:
                    if '$CHARACTER' in l:
                        l = l.replace('$CHARACTER', character)
                    f2.write(l)

if __name__ == '__main__':
    keys = generate_fragmentation()
    generate_mappings(keys)

# https://hub.docker.com/r/alpine/git/tags
FROM alpine/git:v2.32.0
RUN apk update && apk upgrade -a
RUN apk add python3 py3-tqdm py3-requests java-jre-headless java-jdk

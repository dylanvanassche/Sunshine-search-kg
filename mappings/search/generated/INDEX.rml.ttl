@prefix rml: <http://semweb.mmlab.be/ns/rml#> .
@prefix rmlt: <http://semweb.mmlab.be/ns/rml-target#> .
@prefix rr: <http://www.w3.org/ns/r2rml#> .
@prefix ql: <http://semweb.mmlab.be/ns/ql#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix td: <https://www.w3.org/2019/wot/td#> .
@prefix htv: <http://www.w3.org/2011/http#> .
@prefix hctl: <https://www.w3.org/2019/wot/hypermedia#> .
@prefix wotsec: <https://www.w3.org/2019/wot/security#> .
@prefix schema: <http://schema.org/> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix fnml: <http://semweb.mmlab.be/ns/fnml#> .
@prefix fno: <https://w3id.org/function/ontology#> .
@prefix grel: <http://users.ugent.be/~bjdmeest/function/grel.ttl#> .
@prefix void: <http://rdfs.org/ns/void#> .
@prefix idlab: <http://example.com/idlab/function/> .
@prefix formats: <http://www.w3.org/ns/formats/> .
@prefix comp: <http://semweb.mmlab.be/ns/rml-compression#> .
@prefix tree: <https://w3id.org/tree#> .
@base <http://example.org/rules/> .

# Target
<#SearchINDEXTargetDumpGz> a rmlt:LogicalTarget;
  rmlt:target [ a void:Dataset ;
    void:dataDump <file://./kg/search/INDEX.gz>;
  ];
  rmlt:serialization formats:N-Triples;
  rmlt:compression comp:gzip;
.
<#SearchINDEXTargetDump> a rmlt:LogicalTarget;
  rmlt:target [ a void:Dataset ;
    void:dataDump <file://./kg/search/INDEX>;
  ];
  rmlt:serialization formats:N-Triples;
.

# Schema.org AgregateRating
# https://schema.org/AggregateRating
<#SearchINDEXRatingTriplesMap> a rr:TriplesMap;
  rml:logicalSource [ a rml:LogicalSource;
    rml:source "./data/search/INDEX.json";
    rml:referenceFormulation ql:JSONPath;
    rml:iterator "$.data[*]";
  ];

  rr:subjectMap [
    rr:template "rating{id}";
    rr:termType rr:BlankNode;
    rr:class schema:AggregateRating;
    rml:logicalTarget <#SearchINDEXTargetDumpGz>;
    rml:logicalTarget <#SearchINDEXTargetDump>;
  ];

  # Rating is between 0.0 and 1000.0
  rr:predicateObjectMap [
    rr:predicate schema:bestRating;
    rr:objectMap [ rr:constant "1000.0"; rr:datatype xsd:float; ];
  ];

  rr:predicateObjectMap [
    rr:predicate schema:worstRating;
    rr:objectMap [ rr:constant "0.0"; rr:datatype xsd:float; ];
  ];

  rr:predicateObjectMap [
    rr:predicate schema:ratingValue;
    rr:objectMap [ rml:reference "popularity"; rr:datatype xsd:float; ];
  ];
.

<#SearchINDEXTriplesMap> a rr:TriplesMap;
  rml:logicalSource [ a rml:LogicalSource;
    rml:source "./data/search/INDEX.json";
    rml:referenceFormulation ql:JSONPath;
    rml:iterator "$.data[*]";
  ];

  # Search character IRI
  rr:subjectMap [
    rr:template "https://sunshine.dylanvanassche.be/show/{id}";
    rr:class schema:TVSeries;
    rr:class schema:Series;
    rr:class schema:CreativeWorkSeries;
    rml:logicalTarget <#SearchINDEXTargetDumpGz>;
    rml:logicalTarget <#SearchINDEXTargetDump>;
  ];

  # Show name
  rr:predicateObjectMap [
    rr:predicate schema:name;
    rr:objectMap [
      rml:reference "original_name";
      rr:datatype xsd:string;
    ];
  ];

  # Show identifier
  rr:predicateObjectMap [
    rr:predicate schema:identifier;
    rr:objectMap [
      rr:template "https://sunshine.dylanvanassche.be/show/{id}";
      rr:termType rr:IRI;
    ];
  ];

  # Show rating
  rr:predicateObjectMap [
    rr:predicate schema:aggregateRating;
    rr:objectMap [ a rr:RefObjectMap ;
      rr:parentTriplesMap <#SearchINDEXRatingTriplesMap>;
      rr:joinCondition [
        rr:child "id";
        rr:parent "id";
      ];
    ];
  ];
.


# Node relations 
<#SearchINDEXShows> a rr:TriplesMap;
  rml:logicalSource [ a rml:LogicalSource;
    rml:source "./data/search/INDEX.json";
    rml:referenceFormulation ql:JSONPath;
    rml:iterator "$.relations[*]";
  ];

  # Index node IRI
  # Reverse-proxy must serve INDEX.gz as index page for this IRI
  rr:subjectMap [
    rr:constant "https://sunshine.dylanvanassche.be/search/";
    rr:termType rr:IRI;
    rml:logicalTarget <#SearchINDEXTargetDumpGz>;
    rml:logicalTarget <#SearchINDEXTargetDump>;
  ];

  rr:predicateObjectMap [
    rr:predicate tree:relation;
    rr:objectMap [ a rr:RefObjectMap;
      rr:parentTriplesMap <#SearchINDEXStringRelationTriplesMap>;
      rr:joinCondition [
        rr:child "character";
        rr:parent "character";
      ];
    ];
  ];
.

<#SearchINDEXStringRelationTriplesMap> a rr:TriplesMap;
  rml:logicalSource [ a rml:LogicalSource;
    rml:source "./data/search/INDEX.json";
    rml:referenceFormulation ql:JSONPath;
    rml:iterator "$.relations[*]";
  ];

  rr:subjectMap [
    rr:template "{character}";
    rr:termType rr:BlankNode;
    rr:class tree:SubstringRelation;
    rml:logicalTarget <#SearchINDEXTargetDumpGz>;
    rml:logicalTarget <#SearchINDEXTargetDump>;
  ];

  rr:predicateObjectMap [
    rr:predicate tree:node;
    rr:objectMap [
      rr:template "https://sunshine.dylanvanassche.be/search/{character}";
      rr:termType rr:IRI;
    ];
  ];

  rr:predicateObjectMap [
    rr:predicate tree:value;
    rr:objectMap [
      rml:reference "character";
      rr:datatype xsd:string;
    ];
  ];

  rr:predicateObjectMap [
    rr:predicate tree:path;
    rr:objectMap [
      rr:constant schema:name;
      rr:termType rr:IRI;
    ];
  ];
.

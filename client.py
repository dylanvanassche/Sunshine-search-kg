import sys
import requests
import requests_cache
from typing import List, Dict
from time import time
from rdflib import Graph, Namespace, URIRef, RDF
from tempfile import NamedTemporaryFile

SEARCH_INDEX_IRI = 'https://sunshine.dylanvanassche.be/search/'
TREE = Namespace('https://w3id.org/tree#')
SCHEMA = Namespace('http://schema.org/')
RDF_FORMAT = 'ntriples'

def clean_string(string: str) -> str:
    return string.lower().replace(':', '').replace('-', ' ')

def print_results(results: List[Dict]) -> None:
    results = [dict(t) for t in {tuple(d.items()) for d in results}]
    results = sorted(results, key=lambda x: x['rating'], reverse=True)

    print('-' * 80)
    for i, r in enumerate(results):
        print(i+1, r['name'], r['iri'])
    print('-' * 80)

def search(query: str, results: List[Dict], iri: URIRef = URIRef(SEARCH_INDEX_IRI), path = None):
    temp_file: NamedTemporaryFile = NamedTemporaryFile()
    response: Response
    current_node: str = clean_string(str(iri).replace(SEARCH_INDEX_IRI, ''))
    global number_of_requests
    number_of_requests += 1
    g: Graph = Graph()

    # Get fragment from server and parse it
    try:
        print(f'Fragment: {iri}')
        response = requests.get(str(iri), stream=True)
    except Exception:
        print(f'Fragment not found: {iri}')
        print_results(results)
        return number_of_requests

    with open(temp_file.name, 'wb') as f:
        for chunk in response:
            f.write(chunk)
    index = Graph().parse(temp_file, format=RDF_FORMAT)

    # Check if we have results for our query
    if path is not None:
        for s in index.subjects(RDF.type, SCHEMA.TVSeries):
            name = index.value(s, path, any=False)
            rating_iri = index.value(s, SCHEMA.aggregateRating, any=False)
            rating_value = index.value(rating_iri, SCHEMA.ratingValue, any=False)

            if clean_string(name).startswith(query):
                results.append({ 'name': name, 'iri': s, 'rating': rating_value})

        # All nodes visited, stop
        if current_node == query:
            print_results(results)
            return number_of_requests

    # Find next node
    for o in index.objects(iri, TREE.relation):
        value: str = index.value(o, TREE.value, any=False)
        value = clean_string(value)
        if query.startswith(value):
            node: URIRef = index.value(o, TREE.node, any=False)
            path: URIRef = index.value(o, TREE.path, any=False)
            return search(query, results, node, path)

    print_results(results)
    return number_of_requests

if __name__ == '__main__':
    requests_cache.install_cache('http_cache')
    t = time()
    number_of_requests = 0
    q = clean_string(sys.argv[1])
    search(query=q, results=[])
    print(f'Finished in {(time() - t)*1000.0:.2f} ms with '
          f'{number_of_requests} requests')

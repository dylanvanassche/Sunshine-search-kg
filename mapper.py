#!/usr/bin/env python

import requests
import subprocess
import shutil
from pathlib import Path

RMLMAPPER_JAR_URL='https://github.com/RMLio/rmlmapper-java/releases/download/v4.13.0/rmlmapper-4.13.0-r359-all.jar'

class Mapper:
    """
    Wrap the RMLMapper jar as a subprocess.
    """
    def __init__(self, mapper_jar_path: Path):
        self._mapper_jar_path: Path = mapper_jar_path

        # Download RMLMapper Jar if not exist
        if not self._mapper_jar_path.exists():
            with requests.get(RMLMAPPER_JAR_URL, stream=True) as r:
                r.raise_for_status()
                with open(str(self._mapper_jar_path), 'wb') as f:
                    shutil.copyfileobj(r.raw, f)

    def execute(self, mapping_file_path: Path):
        subprocess.call(['java', '-jar', str(self._mapper_jar_path)] \
                        + ['-m', str(mapping_file_path)])


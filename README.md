# Sunshine's search Knowledge Graph (KG)

[Sunshine](https://gitlab.com/DylanVanAssche/Sunshine) 
is a TV show tracking app
which leverages State-of-the-Art Semantic Web technologies 
such as [RML](https://rml.io), 
[schema.org](https://schema.org), 
[TREE](https://tree.linkeddatafragments.org)
to store and search through the user's TV show collection.

## Building the KG

### Generating the RML mapping rules

The RML mapping rules are generated from a [template](mappings/search/char_template.rml.ttl).
This template is filled in by the data from the 
[daily exports](https://developers.themoviedb.org/3/getting-started/daily-file-exports) 
from The Movie DB.
These mapping rules are dumped in the `mappings/search/generated` folder.
All shows are fragmented into several fragments which power the search of Sunshine.
These fragments are dumped as JSON in the `data/search` folder.

```
python3 generate-mappings.py
```

### Building the RDF

After generating the RML mapping rules, they have to be executed.
This is accomplished using the [RMLMapper](https://github.com/RMLio/rmlmapper-java).
The RMLMapper is available as an executable Java jar and executed
over multiple CPU cores using Python's `multiprocessing` module.
Generating the whole Knowledge Graph takes several hours.

```
python3 execute-mappings.py
```

### CI incremental updates

This repository is configured to build the KG every day at 8h15 UTC
since The Movie DB publishes its exports at 8h00 UTC.
Gitlab CI will execute the aformentioned scripts and commit the results 
in the repository.
This only takes a few minutes as the scripts only rebuild the pieces of
the KG which are changed since the last update.

## Deploying your own KG

1. Clone this repository.
2. Update the RML mapping rules template to match with your IRI instead of `https://sunshine.dylanvanassche/search`.
3. Delete the whole KG by deleting the `data`, `mappings/search/generated` and `kg` folders.
4. Execute the steps to build the KG from scratch.
5. Configure your reverse proxy server such as NGINX to use `kg/search` as root directory and `INDEX` as index file.
6. Enjoy!

For Sunshine's own deployment with IRI `https://sunshine.dylanvanassche/search`, 
steps 2,3, and 4 are not applicable.

## License

Everything is licensed under the GPLv3 license except
the content in the `data`, and `kg` folders.
These folders contain data from The MovieDB which
is licensed under their Terms of Service described at 
https://www.themoviedb.org/documentation/api

## TMDB attribution

> This product uses the TMDB API but is not endorsed or certified by TMDB.

![TMDB logo](tmdb.png "TMDB attribution logo.")

